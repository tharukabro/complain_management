﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asp.netCore.Migrations
{
    public partial class Complains : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
             migrationBuilder.CreateTable(
                name: "Complains",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    category_id = table.Column<int>(nullable: false),
                    complain = table.Column<string>(nullable: false),
                    date = table.Column<DateTime>(nullable: false),
                    last_updated = table.Column<DateTime>(nullable: false),
                    document_link = table.Column<string>(nullable: true),
                    nearest_police_station = table.Column<int>(nullable: false),
                    status = table.Column<string>(nullable: true),
                    status_detail = table.Column<string>(nullable: true),
                    user_id = table.Column<int>(nullable: false),
                    close_complain = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Complains", x => x.Id);
                });

                migrationBuilder.AddForeignKey(
                name: "PK_USERID_FORIGN",
                table: "Complains",
                column: "user_id",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

                migrationBuilder.AddForeignKey(
                name: "PK_CATEGORYID_FORIGN",
                table: "Complains",
                column: "category_id",
                principalTable: "Category",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
                migrationBuilder.DropTable(
                name: "Complains");
        }
    }
}
