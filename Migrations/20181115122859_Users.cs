﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asp.netCore.Migrations
{
    public partial class Users : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    account_type = table.Column<bool>(nullable: false),
                    activationCode = table.Column<string>(nullable: true),
                    address = table.Column<string>(nullable: true),
                    civil_status = table.Column<string>(nullable: true),
                    email = table.Column<string>(nullable: true),
                    gender = table.Column<bool>(nullable: false),
                    name = table.Column<string>(maxLength: 30, nullable: false),
                    nearest_police_station = table.Column<int>(nullable: true),
                    nic = table.Column<string>(maxLength: 13, nullable: false),
                    password = table.Column<string>(nullable: true),
                    pro_pic = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
