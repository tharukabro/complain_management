-- USE master
/*GO
IF NOT EXISTS (
    SELECT name
FROM sys.databases
WHERE name = N'complain_management'
)
CREATE DATABASE complain_management
GO*/


-- Create a new table called 'user' in schema 'SchemaName'
-- Drop the table if it already exists

-- Get a list of databases
-- SELECT name
-- FROM sys.databases
-- GO
-- USE master
-- GO
-- IF NOT EXISTS (
--     SELECT name
-- FROM sys.databases
-- WHERE name = N'complain_management'
-- )





-- Drop the database 'cpmplain_managemnt'
-- Connect to the 'master' database to run this snippet
/* USE master
GO */
-- Uncomment the ALTER DATABASE statement below to set the database to SINGLE_USER mode if the drop database command fails because the database is in use.
-- ALTER DATABASE cpmplain_managemnt SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
-- Drop the database if it exists
/* IF EXISTS (
  SELECT name
FROM sys.databases
WHERE name = N'complain_management'
)
DROP DATABASE complain_management
GO

-- Get a list of databases
SELECT name
FROM sys.databases
GO  */



-- USE complain_management
-- GO

-- -- Select rows from a Table or View 'users' in schema 'SchemaName'
-- SELECT *
-- FROM dbo.users
-- GO

-- IF OBJECT_ID('dbo.users', 'U') IS NOT NULL
--     DROP TABLE dbo.users
-- GO
-- CREATE TABLE dbo.users
-- (
--     usersId INT NOT NULL PRIMARY KEY IDENTITY(1,1),
--     -- primary key column
--     name [NVARCHAR](50) NOT NULL,
--     email [NVARCHAR](50) NOT NULL,
--     password [NVARCHAR](80) NOT NULL,
--     address [NVARCHAR](60) NULL,
--     idNumber [NVARCHAR](13) NOT NULL,
--     prof_pic [NVARCHAR](60) NULL,
--     gender [TINYINT] NOT NULL,
--     account_type [TINYINT] NOT NULL,
--     civilStatus [NVARCHAR](20) NULL
--     -- specify more columns here
-- );
-- GO
-- IF OBJECT_ID('dbo.category', 'U') IS NOT NULL
-- DROP TABLE dbo.category
-- GO
-- CREATE TABLE dbo.category
-- (
--     categoryId INT NOT NULL PRIMARY KEY IDENTITY(1,1),
--     -- primary key column
--     category [NVARCHAR](100) NOT NULL,
--     color_code [NVARCHAR](8) NOT NULL

-- );

-- IF OBJECT_ID('dbo.complain', 'U') IS NOT NULL
-- DROP TABLE dbo.complain
-- GO
-- CREATE TABLE dbo.complain
-- (
--     complainId INT NOT NULL PRIMARY KEY IDENTITY(1,1),
--     -- primary key column
--     complain [NVARCHAR](1000) NOT NULL,
--     categoryId [INT] NOT NULL,
--     userId [INT] NOT NULL,
--     status [TINYINT] NULL,
--     date DATETIME NOT NULL,
--     last_updated DATETIME NULL,
--     status_detail [NVARCHAR] NULL,
--     nearestPoliceStation [NVARCHAR](30) NULL,
--     FOREIGN KEY (complainId) REFERENCES category(categoryId),
--     FOREIGN KEY (userId) REFERENCES users(usersId)

-- );

-- IF OBJECT_ID('dbo.news', 'U') IS NOT NULL
-- DROP TABLE dbo.news
-- GO
-- CREATE TABLE dbo.news
-- (
--     newsId INT NOT NULL PRIMARY KEY IDENTITY(1,1),
--     -- primary key column
--     post [NVARCHAR] NOT NULL,
--     feature_image [NVARCHAR](100) NOT NULL,
--     title [NVARCHAR](1000) NOT NULL,
--     date DATETIME NOT NULL
-- );

-- IF OBJECT_ID('dbo.spy', 'U') IS NOT NULL
-- DROP TABLE dbo.spy
-- GO
-- CREATE TABLE dbo.spy
-- (
--     spyId INT NOT NULL PRIMARY KEY IDENTITY(1,1),
--     post [NVARCHAR] NOT NULL,
--     date DATETIME NOT NULL
-- );

-- GO  





-- -- Get a list of databases
-- SELECT name FROM sys.databases
-- GO

-- USE sample
-- GO
-- -- Select rows from a Table or View 'TableOrViewName' in schema 'SchemaName'
-- SELECT * FROM dbo.test	/* add search conditions here */
-- GO


-- SELECT * FROM dbo.admin	/* add search conditions here */
-- GO
-- select count(*) from admin
-- go


-- Select rows from a Table or View 'users' in schema 'SchemaName'
-- USE complain_management
-- GO
-- SELECT *
-- FROM dbo.users
-- GO


-- -- Delete rows from table 'users'
-- DELETE FROM users
-- WHERE usersId>0/* add search conditions here */
-- GO

-- Get a list of tables and views in the current database
-- USE learn_mvc
-- -- Select rows from a Table or View 'Users' in schema 'SchemaName'
-- SELECT *
-- FROM dbo.Users
-- GO

-- Drop the table 'Users' in schema 'SchemaName'
-- IF EXISTS (
--   SELECT *
-- FROM sys.tables
--   JOIN sys.schemas
--   ON sys.tables.schema_id = sys.schemas.schema_id
-- WHERE sys.schemas.name = N'dbo'
--   AND sys.tables.name = N'category'
-- )
--   DROP TABLE dbo.Users
-- GO

-- Select rows from Table or View 'Users' in schema 'SchemaName'
-- SELECT *
-- FROM dbo.Users
-- GO

-- Drop the database 'learn_mvc'
-- Connect to the 'master' database to run this snippet
-- USE master
-- GO
-- -- Uncomment the ALTER DATABASE statement below to set the database to SINGLE_USER mode if the drop database command fails because the database is in use.
-- -- ALTER DATABASE learn_mvc SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
-- -- Drop the database if it exists
-- IF EXISTS (
--   SELECT name
-- FROM sys.databases
-- WHERE name = N'learn_mvc'
-- )
-- DROP DATABASE learn_mvc
-- GO

-- Create a new database called 'learn_mvc'
-- Connect to the 'master' database to run this snippet
-- USE master
-- GO
-- -- Create the new database if it does not exist already
-- IF NOT EXISTS (
--   SELECT name
--     FROM sys.databases
--     WHERE name = N'learn_mvc1'
-- )
-- CREATE DATABASE learn_mvc1
-- GO

-- Drop the database 'learn_mvc1'
-- Connect to the 'master' database to run this snippet
USE master
GO
-- -- Uncomment the ALTER DATABASE statement below to set the database to SINGLE_USER mode if the drop database command fails because the database is in use.
-- -- ALTER DATABASE learn_mvc1 SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
-- -- Drop the database if it exists
IF EXISTS (
  SELECT name
FROM sys.databases
WHERE name = N'learn_mvc1'
)
DROP DATABASE learn_mvc1
GO

-- Create a new database called 'learn_mvc1'
-- Connect to the 'master' database to run this snippet
USE master
GO
-- -- Create the new database if it does not exist already
IF NOT EXISTS (
  SELECT name
FROM sys.databases
WHERE name = N'learn_mvc1'
)
CREATE DATABASE learn_mvc1
GO

USE learn_mvc1
-- Get a list of tables and views in the current database
SELECT table_catalog [database], table_schema [schema], table_name name, table_type type
FROM INFORMATION_SCHEMA.TABLES
GO




-- USE learn_mvc1
-- -- Select rows from a Table or View 'Category' in schema 'SchemaName'
-- SELECT *
-- FROM dbo.Category
-- GO