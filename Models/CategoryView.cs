using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace Asp.net_Core.Models
{
    public class CategoryView
    {
        public int Id { get; set; }

        [Required]
        public string categoryName { get; set; }
       [Required]
        public string color { get; set; }

    }
}