using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace Asp.net_Core.Models
{
    public class AdminDetail
    {
        public int Id { get; set; }


        public string policeIDNum { get; set; }

       
        public string workingStation { get; set; }

        public string rank { get; set; }

        public int userId{ get; set; }


    }
}