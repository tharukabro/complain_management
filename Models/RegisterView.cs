using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace Asp.net_Core.Models
{
    public class RegisterView
    {

        [Required]
        public string name { get; set; }

        [Required, EmailAddress]
        public string email { get; set; }

        [Required]
        public string password { get; set; }

        [Required, Compare("password")]
        public string confirmPassword { get; set; }

        [Required]
        public bool gender { get; set; }

        [Required]
        public string nic { get; set; }

        [Required]
        public string accept { get; set; }
        
    }
}