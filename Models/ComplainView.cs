using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace Asp.net_Core.Models
{
    public class ComplainView
    {
        public Category categoryVm { get; set; }
        public Complains complainVm { get; set; }

        public PoliceStations stationVm { get; set; }
    }
}
