using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Asp.net_Core.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options):base(options)
        {
            Database.Migrate();
        }

        public DbSet<Users> Users {get; set;}

        public DbSet<Category> Category {get; set;}

        public DbSet<Complains> Complains {get; set;}

        public DbSet<News> News {get; set;}

        public DbSet<Spy> Spy {get; set;}

        public DbSet<PoliceStations> PoliceStations {get; set;}
        public DbSet<AdminDetail> AdminDetail { get; set; }

    }
}