using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace Asp.net_Core.Models
{
    public class Complains
    {
        public int Id { get; set; }

        [Required]
        public string complain { get; set; }

        [Required]
        public int category_id { get; set; }

        public int user_id { get; set; }

        public string status { get; set; }

        public DateTime date { get; set; }

        public DateTime last_updated { get; set; }

        public string status_detail { get; set; }

        public bool close_complain { get; set; }

        public string document_link { get; set; }

        [Required]
        public int nearest_police_station { get; set; }

    }
}