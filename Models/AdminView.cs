using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace Asp.net_Core.Models
{
    public class AdminView
    {
        public AdminDetail admin{get; set;}
        public Users users{get; set;}
    }
}