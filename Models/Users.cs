using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Asp.net_Core.Models
{
    public class Users
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Make sure you are entered the Name")]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "Oops it seems you are doing some thing wrong!")]
        public string name { get; set; }

       
        public string email { get; set; }

        public string password { get; set; }

        [Required(ErrorMessage = "Oops select your gender")]
        public bool gender { get; set; }

        [Required(ErrorMessage = "Please enter the NIC number")]
        [StringLength(13, MinimumLength = 10, ErrorMessage = "Please enter valid NIC number")]
        public string nic { get; set; }

        public bool account_type { get; set; }

        public string civil_status { get; set; }

        public string pro_pic { get; set; }

        public string address { get; set; }

        public string nearest_police_station { get; set; }

        public string activationCode{ get; set; }
    }
}