using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace Asp.net_Core.Models
{
    public class News
    {
        public int Id { get; set; }

        [Required]
        public string post { get; set; }

        [Required]
        public string feature_image { get; set; }

        [Required]
        public string title { get; set; }

        public DateTime date { get; set; }

    }
}