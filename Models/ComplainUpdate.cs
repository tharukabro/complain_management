using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace Asp.net_Core.Models
{
    public class ComplainUpdate
    {
        public int id { get; set; }
        public string message{ get; set; }

        [Required]
        public string status{ get; set; }
    }
}
