/* USE learn_mvc1
-- Select rows from a Table or View 'users' in schema 'SchemaName'
SELECT *
FROM dbo.users
GO */

-- Get a list of tables and views in the current learn_mvc1
SELECT table_catalog [learn_mvc1], table_schema [dbo], table_name name, table_type type
FROM INFORMATION_SCHEMA.TABLES
GO