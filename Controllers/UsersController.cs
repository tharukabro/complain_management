using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Asp.net_Core.Models;
using Microsoft.AspNetCore.Authentication;  
using Microsoft.AspNetCore.Authorization;
using System.Text;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.Extensions.Configuration;
using System.Security.Claims;
using static Microsoft.AspNetCore.Hosting.Internal.HostingApplication;
using Microsoft.AspNetCore.Http;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net;
using System.Net.Mail;

namespace Asp.net_Core.Controllers
{
    public class UsersController : Controller
    {

        private readonly IHostingEnvironment _appEnvironment;
        public static IConfiguration Configuration { get; set; }  


        private readonly ApplicationDbContext _db;

        public UsersController(ApplicationDbContext db, IHostingEnvironment appEnvironment)
        {
            _db = db;
            _appEnvironment = appEnvironment;  


            checkEmail();

        }
        [Authorize] 
        public IActionResult index()
        {
            return RedirectToAction(nameof(Home));
            //return View(_db.Users.ToList());
        }

        private IActionResult checkEmail()
        {
            return View();
        }


        public IActionResult View(int? id)
        {
            if(id==null)
            {
                return NotFound();
            }
             var user  = _db.Users.SingleOrDefault(m => m.Id == id);
            if(user==null)
            {
                return NotFound();
            }
            return View(user);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if(id==null)
            {
                return NotFound();
            }
            var user  = _db.Users.SingleOrDefault(m => m.Id == id);
            if(user==null)
            {
                return NotFound();
            }
            _db.Remove(user);
            await _db.SaveChangesAsync();
            return RedirectToAction(nameof(index));
        }

         public IActionResult Update(int? id)
        {
            if(id==null)
            {
                return NotFound();
            }
             var user  = _db.Users.SingleOrDefault(m => m.Id == id);
            if(user==null)
            {
                return NotFound();
            }
            return View(user);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, Users user)
        {
            if(id!=user.Id)
            {
                return NotFound();
            }   
            if(ModelState.IsValid)
            {
                _db.Update(user);
                await _db.SaveChangesAsync();
                return RedirectToAction(nameof(index));
            }
            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EmailActive(string email, string code)
        {
            Users _model  = _db.Users.SingleOrDefault(m => m.email == email && m.activationCode == code);
            if(_model!=null)
            {
                _model.activationCode = "activated";
                _db.Update(_model);
                await _db.SaveChangesAsync();
                TempData["AdminRegisterSuccess"]="You have activated your email successfully!";
                return RedirectToAction(nameof(Login));
            }
            TempData["error"]="Wrong activation code!";
            return View("ActivateEmail");
        }

        public async Task<IActionResult> resendEmail(string email)
        {
            Random random = new Random();
            string code = random.Next(100000, 999999).ToString();  
            Users _model  = _db.Users.SingleOrDefault(m => m.email == email);
            if(_model==null)
            {
                return RedirectToAction("Login","users");
            }

            sendEmail("tharukawapnet@gmail.com", email, code );

            _model.activationCode = code;
            _db.Update(_model);
            await _db.SaveChangesAsync();

            ViewData["email"] = email;
            TempData["sendSuccess"] = "Activation code sent successfully!";
            return View("ActivateEmail");
        } 

        [Authorize]
        private Boolean accountType()
        {
            var user_id = _db.Users.FirstOrDefault(m => m.email == User.Identity.Name).account_type;
            if(!user_id)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginView user)
        {
   
            if(ModelState.IsValid)
            {

                 /* Getting salt value from the appsettings.json */
                //string salt_value = Configuration["salt:data"]; 

                /* Line 126-131 hashing the password */
                string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: user.password,
                salt: Encoding.Unicode.GetBytes("7DaRsmHp4y"),
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8)); 

                //Users _model  = _db.Users.Where(" ");
                Users _model  = _db.Users.SingleOrDefault(m => m.email == user.email && m.password == hashed && m.account_type==false);
                if(_model==null)
                {
                    TempData["AdminLoginFailed"] = "Login Failed.Please enter correct credentials";
                    return View();
                }

                string code = _model.activationCode;
                if(code!="activated")
                {
                    ViewData["email"] = user.email;
                    return View("ActivateEmail");
                }
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, _model.email, ClaimValueTypes.String),
                    new Claim(ClaimTypes.Role, "user", ClaimValueTypes.String)
                };
                ClaimsIdentity identity = new ClaimsIdentity(claims, "login_user");
                ClaimsPrincipal principal = new ClaimsPrincipal(identity);

                await HttpContext.SignInAsync(principal);
                return RedirectToAction("home","users");
                
            }
            return View(user);
        }


        public IActionResult Login()
        {
            if(User.Identity.IsAuthenticated)
            {
                // bool _model  = _db.Users.SingleOrDefault(m => m.email == User.Identity.Name &&  m.account_type==true).account_type;
                // if(_model)
                // {
                //     return RedirectToAction("index","admins");
                // }
                return RedirectToAction("index","home");
            }
            return View();
        }

        public IActionResult changeLanguage()
        {
            if(Request.Cookies["lan"]==null)
            {
                setLanguage("lan", "si");
                return Content("NUll");
            }else if(Request.Cookies["lan"]=="si")
            {
                setLanguage("lan", "en");
                return Content("Sinhala");
            }else{
                setLanguage("lan", "si");
                return Content("English");
            }
            return RedirectToAction("home");
        }

        public void setLanguage(string key, string value)  
        {  
            CookieOptions option = new CookieOptions();  
            option.Expires = DateTime.Now.AddMilliseconds(30);    
            Response.Cookies.Append(key, value, option);  
        }   

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterView user)
        {
   
            if(ModelState.IsValid)
            {


                //Users _model  = _db.Users.Where(" ");
                Users _model  = _db.Users.SingleOrDefault(m => m.email == user.email);
                if(_model!=null)
                {
                    TempData["AdminLoginFailed"] = "Already have account with this email!";
                    return View();
                }

                 /* Getting salt value from the appsettings.json */
                //string salt_value = Configuration["salt:data"]; 

                /* Line 126-131 hashing the password */
                Random random = new Random(); 
                string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: user.password,
                salt: Encoding.Unicode.GetBytes("7DaRsmHp4y"),
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8)); 

                string code = random.Next(100000, 999999).ToString();  

                Users _user = new Users();
                _user.name = user.name;
                _user.email = user.email;
                _user.password = hashed;
                _user.gender = user.gender;
                _user.account_type = false;
                _user.nic = user.nic;
                _user.pro_pic = "pro_pic.png";
                _user.activationCode = code;

                sendEmail("tharukawapnet@gmail.com",user.email,code);
                _db.Add(_user);
                await _db.SaveChangesAsync();  
                TempData["AdminRegisterSuccess"] = "You have successfully registered! Login now.";              
                return RedirectToAction("login","users");
                
            }
            return View(user);
        }

        public IActionResult Register()
        {
        
            if(User.Identity.IsAuthenticated)
            {
                bool _model  = _db.Users.SingleOrDefault(m => m.email == User.Identity.Name &&  m.account_type==true).account_type;
                if(_model)
                {
                    return RedirectToAction("index","admins");
                }
                return RedirectToAction("index","users");
            }
            return View();
        }

        [Authorize]
        public IActionResult Home()
        {
            if(!accountType())
            {
                return RedirectToAction("index","admins");
            }
            var user_id = _db.Users.FirstOrDefault(m => m.email == User.Identity.Name).Id;
            var _data = from s in _db.Complains
                        join st in _db.Category on s.category_id equals st.Id
                        join u in _db.Users on s.user_id equals u.Id
                        join p in _db.PoliceStations on s.nearest_police_station equals p.Id
                        where s.user_id == user_id
                        select new AdminComplainsView { complainVm = s, categoryVm = st, userVm = u, stationVm = p };
            
            ViewData["user_"] = getUserDetail();
            ViewData["complains"] = _data;
            return View();
        }

        [Authorize] 
        [HttpGet]  
        public async Task<IActionResult> logout()  
        {  
            await HttpContext.SignOutAsync();  
            return RedirectToAction("login", "users");  
        }  

        [Authorize]
        public IActionResult NewComplain()
        {
            
                ViewData["headTitle"] = "Add New Complain";
                ViewData["subTitle"] = "Add your complain here";
                ViewData["complain"] = "Complain";
                ViewData["complain_placeHolder"] = "Insert your complain";
                ViewData["CategoryMsg"] = "Category";
                ViewData["nearest"] = "Nearest Police Station";
                ViewData["pdf"] = "If you have a lots of images, and informations please make them as pdf.";
                ViewData["video"] = "If you have video files. Please kindly upload that videos to DropBox/Google Drive/One Drive and paste the link in the description box.";
            if(!accountType())
            {
                return RedirectToAction("index","admins");
            }
            Users _model  = _db.Users.FirstOrDefault(m => m.email == User.Identity.Name);//User.Identity.Name
            
            ViewData["locations"] = _db.PoliceStations.ToList();
            ViewData["category"] = _db.Category.ToList();
            ViewData["user_"] = getUserDetail();
            return View();
        }

        public IActionResult newComplainSi()
        {
            if(!accountType())
            {
                return RedirectToAction("index","admins");
            }
            Users _model  = _db.Users.FirstOrDefault(m => m.email == User.Identity.Name);//User.Identity.Name
            

                ViewData["headTitle"] = "අලුත් පැමිණිල්ලක් ඇතුලත් කරන්න";
                ViewData["subTitle"] = "ඔබගේ පැමිණිල්ල මෙහි ඇතුලත් කරන්න";
                ViewData["complain"] = "පැමිණිල්ල";
                ViewData["complain_placeHolder"] = "ඔබගේ පැමිණිල්ල ඇතුලත් කරන්න";
                ViewData["Category"] = "කාණ්ඩය";
                ViewData["nearest"] = "ළගම පොලිස් ස්තානය";
                ViewData["pdf"] = "ඔබ සතුව ඡායාරූප කිහිපයක් ඇත්නම් සහ තවත් තොරතුරු තිබේනම්, PDF ගොනුවක් සේ සකසා මෙහි ඇතුලත් කරන්න.";
                ViewData["video"] = "ඔබ සතුව වීඩියෝ ප්වතී නම් ඒවා, Google Drive/One Drive/DropBox වැනි සේවාවක අප්ලෝඩ් කර Description හි එහි link ඇතුලත් කරන්න.";

            ViewData["locations"] = _db.PoliceStations.ToList();
            ViewData["category"] = _db.Category.ToList();
            ViewData["user_"] = getUserDetail();
            return View("NewComplain");
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> NewComplain(Complains user, IFormFile files)
        {


            ViewData["category"] = _db.Category.ToList();
            ViewData["locations"] = _db.PoliceStations.ToList();
            ViewData["user_"] = getUserDetail();
            if(ModelState.IsValid)
            {   
                user.close_complain = false;
                user.last_updated = DateTime.Now;
                user.date = DateTime.Now;
                user.status = "Pending";
                user.user_id =  _db.Users.FirstOrDefault(m => m.email == User.Identity.Name).Id;
                           

                
                if (!(null==files))
                {
                    string fileName = DateTime.Now.ToString("FFF")+files.FileName;
                        var uploads = Path.Combine(_appEnvironment.WebRootPath, "complainFiles");
                        var filePath = Path.Combine(uploads, fileName);
                        
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await files.CopyToAsync(stream);
                    }
                    user.document_link = fileName;
                }
                

                TempData["SuccessInserted"] = "Your complain reached us!";         
                _db.Add(user);
                await _db.SaveChangesAsync();
                return View();
            }
            return View(user);
        }


        [Authorize]
        public async Task<IActionResult> CloseComplain(int? id)
        {
            if(id==null)
            {
                return NotFound();
            }
            var user_id = _db.Users.FirstOrDefault(m => m.email == User.Identity.Name).Id;
            var complain  = _db.Complains.SingleOrDefault(m => m.Id == id && m.user_id == user_id);
            if(complain==null)
            {
                return NotFound();
            }

            complain.close_complain = true;
            _db.Update(complain);
            await _db.SaveChangesAsync();

            ViewData["category"] = _db.Category.ToList();
            ViewData["user_"] = getUserDetail();
            //return View();
            return RedirectToAction(nameof(Home));
        }


        [Authorize]
        public IActionResult ViewComplain(int? id)
        {
            if(id==null)
            {
                return NotFound();
            }
            var user_id = _db.Users.FirstOrDefault(m => m.email == User.Identity.Name).Id;
            // var _data = _db.Complains.Where(m=>m.Id == id && m.user_id == user_id).FirstOrDefault();
            var data =  from s in _db.Complains
                        join st in _db.Category on s.category_id equals st.Id into st2
                        join p in _db.PoliceStations on s.nearest_police_station equals p.Id
                        from st in st2.DefaultIfEmpty()
                        where s.user_id == user_id && s.Id == id
                        select new ComplainView { complainVm = s, categoryVm = st, stationVm = p };
            if(data==null)
            {
                return NotFound();
            }
            
            ViewData["user_"] = getUserDetail();
            ViewData["complain"] = data.FirstOrDefault();
            return View();
        }

        [Authorize]
        public IActionResult Profile()
        {
            if(!accountType())
            {
                return RedirectToAction("index","admins");
            }
            Users user = _db.Users.FirstOrDefault(m => m.email == User.Identity.Name);
            return View(user);
        }

        [Authorize]
        public IActionResult Messages()
        {
            if(!accountType())
            {
                return RedirectToAction("index","admins");
            }
            ViewData["user_"] = getUserDetail();
            return View();
        }


        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Profile(Users user, ICollection<IFormFile> files)
        {
            /* if(!accountType())
            {
                return RedirectToAction("index","admins");
            } */
            if(ModelState.IsValid)
            {
                
                foreach (var file in files)
                {
                    if (file.Length > 0)
                    {

                        if (file.ContentType.IndexOf("image", StringComparison.OrdinalIgnoreCase) < 0)
                        {
                            TempData["image_error"] = "Error:This file is not an image";
                            return View(getUserDetail());
                        }

                        string oldImage = _appEnvironment.WebRootPath+"/profile_pics/"+user.pro_pic;

                        //return Content(oldImage);
                        //string fullPath = Request.MapPath("~/Images/Cakes/" + photoName);
                        if (System.IO.File.Exists(oldImage))
                        {
                            System.IO.File.Delete(oldImage);
                        }

                        
                        //var returned = File.Delete();

                        string fileName = DateTime.Now.ToString("FFF")+file.FileName;
                        var uploads = Path.Combine(_appEnvironment.WebRootPath, "profile_pics");
                        var filePath = Path.Combine(uploads, fileName);
                        using (var fileStream = new FileStream(filePath, FileMode.Create)) {
                            await file.CopyToAsync(fileStream);
                        };
                        user.pro_pic = fileName;

                    }
                    
                    break;
                }  
                TempData["update_success"] = "Profile details updated successfully!";
                _db.Users.Update(user);
                await _db.SaveChangesAsync();
            }            
            return View(user);
        }


        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult changePassword(string old, string newPswrd)
        {
            if(old==null || newPswrd==null)
            {
                TempData["pswrd_error"] = "Are you sure you have typed new and old passwords?";
                return RedirectToAction(nameof(Profile));
            }
            
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
            password: old,
            salt: Encoding.Unicode.GetBytes("7DaRsmHp4y"),
            prf: KeyDerivationPrf.HMACSHA1,
            iterationCount: 10000,
            numBytesRequested: 256 / 8)); 

            Users user = getUserDetail();
            string currentPswrd = user.password;

            if(hashed!=currentPswrd)
            {
                TempData["pswrd_error"] = "Entered current password is invalid?";
                return RedirectToAction(nameof(Profile));
            }

            hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
            password: newPswrd,
            salt: Encoding.Unicode.GetBytes("7DaRsmHp4y"),
            prf: KeyDerivationPrf.HMACSHA1,
            iterationCount: 10000,
            numBytesRequested: 256 / 8));

            user.password = hashed;
            user.activationCode = "activated";
            _db.Users.Update(user);
            _db.SaveChangesAsync();
            
            TempData["pswrd_success"] = "Password Changed Successfully!";
            return RedirectToAction(nameof(Profile));
        }
        /* [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Profile(Users user)
        {
            if(ModelState.IsValid)
            {
                _db.Users.Update(user);
                await _db.SaveChangesAsync();
            }    
            return View(user);
        } */
 
        [Authorize]
        private Users getUserDetail()
        {
           return _db.Users.FirstOrDefault(m => m.email == User.Identity.Name);//User.Identity.Name
        }


        [Authorize]
        private IActionResult checkUserType()
        {
            Users _user = _db.Users.FirstOrDefault(m => m.email == User.Identity.Name);
            if(_user.account_type)
            {
                return View();
            }
            return View();
        }

        private async void sendEmail(string from, string to, string code)
        {
            var smtpClient = new SmtpClient
            {
                Host = "smtp.gmail.com", // set your SMTP server name here
                Port = 587, // Port 
                EnableSsl = true,
                Credentials = new NetworkCredential("erandam5s@gmail.com", "eranda961230")
            };

            using (var message = new MailMessage(from, to)
            {
                Subject = "Confirm your Email",
                Body = "<h3>You Registered successfully</h3><P>"+code+"</p>"
            })
            {
                message.IsBodyHtml = true;
                await smtpClient.SendMailAsync(message);
            }
        }
        protected override void Dispose(bool disposing)
        {
            if(disposing)
            {
                _db.Dispose();
            }
        }
    }
}
