using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Asp.net_Core.Models;
using Microsoft.AspNetCore.Authentication;  
using Microsoft.AspNetCore.Authorization;
using System.Text;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.Extensions.Configuration;
using System.Security.Claims;
using static Microsoft.AspNetCore.Hosting.Internal.HostingApplication;
using Microsoft.AspNetCore.Http;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Linq.Dynamic.Core;

namespace Asp.net_Core.Controllers
{
    public class AdminsController : Controller
    {
        private readonly IHostingEnvironment _appEnvironment;
        public static IConfiguration Configuration { get; set; }  

        private readonly ApplicationDbContext _db;


        public AdminsController(ApplicationDbContext db, IHostingEnvironment appEnvironment)
        {
            _db = db;
            _appEnvironment = appEnvironment;  

            /* 
                * Default Email: tharukawapnet@gmail.com
                * Default Password: 12345678
             */
            //makeDefaultAdmin();

        }

        private void makeDefaultAdmin()
        {
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: "12345678",
                salt: Encoding.Unicode.GetBytes("7DaRsmHp4y"),
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));
            Users _model  = _db.Users.SingleOrDefault(m => m.email == "default@admin.com" && m.account_type==true);
            if(_model==null)
            {
                _model = new Users();
                _model.name = "Main Admin";
                _model.email = "default@admin.com";
                _model.password = hashed;
                _model.gender = true;
                _model.nic = "000000000v";
                _model.account_type = true;
                _model.activationCode = "activated";

                _db.Add(_model);
                _db.SaveChangesAsync();
            }
        }


        public IActionResult tips()
        {
            return View();
        }
        public IActionResult loadPolice()
        {   
            try  
            {  
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();  
                // Skiping number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();  
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();  
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();  
                // Sort Column Direction ( asc ,desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();  
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();  
  
                //Paging Size (10,20,50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;  
                int skip = start != null ? Convert.ToInt32(start) : 0;  
                int recordsTotal = 0;  
  
                // Getting all Customer data  
                //var userData = (from users in _db.Users  select users);
                var userData = _db.PoliceStations.Where(m=> m.Id>0);
  
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))  
                {  
                    userData = userData.OrderBy(sortColumn + " " + sortColumnDirection);  
                    //userData = userData.OrderBy(sortColumn);
                }  
                //Search  
                if (!string.IsNullOrEmpty(searchValue))  
                {  
                    userData = userData.Where(m => m.name.Contains(searchValue));  
                }  
  
                //total number of rows count   
                recordsTotal = userData.Count();  
                //Paging   
                var data = userData.Skip(skip).Take(pageSize).ToList();  
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });  
  
            }  
            catch (Exception e)  
            {  
                return Content(e.Message);  
            }  
        }

        public IActionResult Index()
        {
            return RedirectToAction("dashboard","admins");
        }

        [Authorize] 
        [HttpGet]  
        public async Task<IActionResult> logout()  
        {  
            await HttpContext.SignOutAsync();  
            return RedirectToAction("login", "admins");  
        }

        [Authorize]
        public IActionResult dashboard()
        {
            var _data = from s in _db.Complains
                        join st in _db.Category on s.category_id equals st.Id
                        join u in _db.Users on s.user_id equals u.Id
                        join p in _db.PoliceStations on s.nearest_police_station equals p.Id
                        select new AdminComplainsView { complainVm = s, categoryVm = st, userVm = u, stationVm = p };
            ViewData["complains"] = _data;
            return View();
        }

        public IActionResult users()
        {
            return View();
        }

        public IActionResult loadUsers()
        {
            try  
            {  
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();  
                // Skiping number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();  
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();  
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();  
                // Sort Column Direction ( asc ,desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();  
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();  
  
                //Paging Size (10,20,50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;  
                int skip = start != null ? Convert.ToInt32(start) : 0;  
                int recordsTotal = 0;  
  
                // Getting all Customer data  
                //var userData = (from users in _db.Users  select users);
                var userData = _db.Users.Where(m=> m.account_type==false);
  
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))  
                {  
                    userData = userData.OrderBy(sortColumn + " " + sortColumnDirection);  
                    //userData = userData.OrderBy(sortColumn);
                }  
                //Search  
                if (!string.IsNullOrEmpty(searchValue))  
                {  
                    userData = userData.Where(m => m.name.Contains(searchValue) || m.email.Contains(searchValue) || m.address.Contains(searchValue) || m.nic.Contains(searchValue));  
                }  
  
                //total number of rows count   
                recordsTotal = userData.Count();  
                //Paging   
                var data = userData.Skip(skip).Take(pageSize).ToList();  
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });  
  
            }  
            catch (Exception)  
            {  
                throw;  
            }  
        }

        public IActionResult loadAdmin()
        {
            try  
            {  
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();  
                // Skiping number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();  
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();  
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();  
                // Sort Column Direction ( asc ,desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();  
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();  
  
                //Paging Size (10,20,50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;  
                int skip = start != null ? Convert.ToInt32(start) : 0;  
                int recordsTotal = 0;  
  
                // Getting all Customer data  
                //var userData = (from users in _db.Users  select users);
                var userData = from s in _db.Users
                                join st in _db.AdminDetail on s.Id equals st.userId
                                select new AdminView { users = s, admin = st };
                //userData = userData.Where(m=> m.users.account_type==false);
  
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))  
                {  
                    userData = userData.OrderBy(sortColumn + " " + sortColumnDirection);  
                    //userData = userData.OrderBy(sortColumn);
                }  
                //Search  
                if (!string.IsNullOrEmpty(searchValue))  
                {  
                    userData = userData.Where(m => m.users.name.Contains(searchValue) || m.users.email.Contains(searchValue) || m.admin.policeIDNum.Contains(searchValue) || m.users.nic.Contains(searchValue));  
                }  
  
                //total number of rows count   
                recordsTotal = userData.Count();  
                //Paging   
                var data = userData.Skip(skip).Take(pageSize).ToList();  
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });  
  
            }  
            catch (Exception)  
            {  
                throw;  
            }  
        }

        public IActionResult allAdmin()
        {
            return View();
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> adminLists(NewAdminView _model)
        {
            if(ModelState.IsValid)
            {

                Users check  = _db.Users.SingleOrDefault(m => m.email == _model.email && m.account_type==true);
                if(check!=null)
                {
                    TempData["AdminLoginFailed"] = "Already have account with this email!";
                    return View();
                }

                Random random = new Random(); 
                string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: _model.password,
                salt: Encoding.Unicode.GetBytes("7DaRsmHp4y"),
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8)); 

                var user  = new Users();
                user.name = _model.name;
                user.email = _model.email;
                user.password = hashed;
                user.gender = _model.gender;
                user.nic = _model.nic;
                user.account_type = true;
                user.activationCode = "activated";

                _db.Add(user);
                await _db.SaveChangesAsync();

                user = _db.Users.Where(m=> m.email==_model.email).FirstOrDefault();

                var admin = new AdminDetail();
                admin.policeIDNum = _model.serviceId;
                admin.userId = user.Id;
                _db.Add(admin);
                await _db.SaveChangesAsync();

                TempData["success"] = "New admin added successfully!";
                return View();
            }
            return View(_model);
        }

        public IActionResult policeStations(int? id)
        {
            if(id==null)
            {
                return View();
            }
            ViewData["editData"] = "edit";
            var _data = _db.PoliceStations.SingleOrDefault(m=> m.Id==id);
            if(_data==null)
            {
                return NoContent();
            }
            return View(_data);
            
        }


        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> updatePoliceStations(PoliceStations _model ,int? id)
        {
            if(id==null)
            {
                return View();
            }

            _db.Update(_model);
            await _db.SaveChangesAsync();
            TempData["success"] = "Police Station Name Updated Successfully!";
            return RedirectToAction("policeStations");
            
        }

        public async Task<IActionResult> deleteUser(int? id)
        {
            if(id==null)
            {
                return NoContent();
            }
            var _data = _db.Users.Where(m=> m.Id==id && m.account_type==false).SingleOrDefault();
            if(_data==null)
            {
                return NoContent();
            }
            _db.Remove(_data);
            await _db.SaveChangesAsync();
            return RedirectToAction(nameof(users));
        }

        public IActionResult category()
        {
            var _data = _db.Category.ToList();
            ViewData["_data"] =  _data;
            return View();
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> updateComplain(ComplainUpdate complain)
        {
            var _data = _db.Complains.FirstOrDefault(m=> m.Id == complain.id);
            if(ModelState.IsValid)
            {
                if(_data==null)
                {
                    return NotFound();
                }
                _data.status_detail = complain.message;
                _data.status = complain.status;

                _db.Update(_data);
                await _db.SaveChangesAsync();

                return RedirectToAction("viewComplain", new { @id = complain.id });
                //return ActionResult(viewComplain(1));
            }
            return NotFound();
        }

        public IActionResult news()
        {
            return View();
        }

        public IActionResult deleteAdmin(int? id)
        {
            if(id==null)
            {
                return NoContent();
            }
            var _user = _db.Users.SingleOrDefault(m=>m.Id==id && m.account_type==true);
            if(_user==null)
            {   
                return NoContent();
            }
            _db.Remove(_user);
            _db.SaveChangesAsync();
            TempData["success"] = "User Deleted Successfully";
            return RedirectToAction("allAdmin");
        }

        public IActionResult adminLists()
        {
            return View();
        }

       /*  [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> adminLists()
        {

        } */

        public IActionResult viewComplain(int? id)
        {
            ComplainUpdate _dataC = new ComplainUpdate();
            if(id==null)
            {
                return NotFound();
            }
            var _data = from s in _db.Complains
                        join st in _db.Category on s.category_id equals st.Id
                        join u in _db.Users on s.user_id equals u.Id
                        join p in _db.PoliceStations on s.nearest_police_station equals p.Id
                        where s.Id == id
                        select new AdminComplainsView { complainVm = s, categoryVm = st, userVm = u, stationVm = p };
            if(_data==null)
            {
                return NotFound();
            }
            ViewData["complain"] = _data.SingleOrDefault();
            _dataC.message = _data.SingleOrDefault().complainVm.status_detail;
            _dataC.status = _data.SingleOrDefault().complainVm.status;
            _dataC.id = _data.SingleOrDefault().complainVm.Id;
            return View(_dataC);
        }

        [Authorize]
        private Users getUserDetail()
        {
           return _db.Users.FirstOrDefault(m => m.email == User.Identity.Name);//User.Identity.Name
        }

        [Authorize]
        public async Task<IActionResult> deleteCat(int? id)
        {
            if(id==null)
            {
                return NoContent();
            }

            Category category = _db.Category.SingleOrDefault(m=> m.Id==id);
            if(category!=null)
            {
                 _db.Remove(category);
                await _db.SaveChangesAsync();

                TempData["deleted"] = "Category Deleted Successfully!";
                return RedirectToAction(nameof(category));
            }
           
            return NoContent();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> category(Category category)
        {
            if(ModelState.IsValid)
            {
                TempData["success"] = "New Category Inserted successfuly!";
                _db.Add(category);
                await _db.SaveChangesAsync();
                return RedirectToAction(nameof(category));
            }
            return View(category);
        }

        public IActionResult deleteSpy(int? id)
        {
            if(id==null)
            {
                return NoContent();
            }
            var _data = _db.Spy.FirstOrDefault(m=> m.Id ==id);
            if(_data==null)
            {
                return NoContent();
            } 
            _db.Remove(_data);
            _db.SaveChangesAsync();
            TempData["success"] = "Tip Deleted successfuly!!";
            return RedirectToAction("tips");  
        }
        public IActionResult loadTips()
        {
            try  
            {  
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();  
                // Skiping number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();  
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();  
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();  
                // Sort Column Direction ( asc ,desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();  
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();  
  
                //Paging Size (10,20,50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;  
                int skip = start != null ? Convert.ToInt32(start) : 0;  
                int recordsTotal = 0;  
  
                // Getting all Customer data  
                //var userData = (from users in _db.Users  select users);
                var userData = _db.Spy.Where(m=> m.Id>0);
                //userData = userData.Where(m=> m.users.account_type==false);
  
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))  
                {  
                    userData = userData.OrderBy(sortColumn + " " + sortColumnDirection);  
                    //userData = userData.OrderBy(sortColumn);
                }  
                //Search  
                if (!string.IsNullOrEmpty(searchValue))  
                {  
                    userData = userData.Where(m => m.post.Contains(searchValue) || m.post.Contains(searchValue));  
                }  
  
                //total number of rows count   
                recordsTotal = userData.Count();  
                //Paging   
                var data = userData.Skip(skip).Take(pageSize).ToList();  
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });  
  
            }  
            catch (Exception)  
            {  
                throw;  
            }  
        }

        

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> policeStations(PoliceStations _model)
        {   
            if(ModelState.IsValid)
            {
                TempData["success"] = "New Police Station Added Successfully";
                _db.Add(_model);
                await _db.SaveChangesAsync();
                return RedirectToAction("policeStations");
            }
            return View(_model);
        }

        public IActionResult login()
        {
            makeDefaultAdmin();
            if(User.Identity.IsAuthenticated)
            {
                if(accountType())
                {
                    return RedirectToAction("home","users");
                }
                return RedirectToAction("dashboard","admins");
                
            }
            return View();
        }

        [Authorize]
        private Boolean accountType()
        {
            var user_id = _db.Users.FirstOrDefault(m => m.email == User.Identity.Name).account_type;
            if(!user_id)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> login(LoginView user)
        {
            
            if(ModelState.IsValid)
            {

                /* Getting salt value from the appsettings.json */
                //string salt_value = Configuration["salt:data"]; 

                /* Line 126-131 hashing the password */
                string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: user.password,
                salt: Encoding.Unicode.GetBytes("7DaRsmHp4y"),
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8)); 

                //Users _model  = _db.Users.Where(" ");
                Users _model  = _db.Users.SingleOrDefault(m => m.email == user.email && m.password == hashed && m.account_type==true);
                if(_model==null)
                {
                    TempData["AdminLoginFailed"] = "Login Failed.Please enter correct credentials";
                    return View();
                }

                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, _model.email, ClaimValueTypes.String),
                    new Claim(ClaimTypes.Role, "admin", ClaimValueTypes.String)
                };
                ClaimsIdentity identity = new ClaimsIdentity(claims, "login_admin");
                ClaimsPrincipal principal = new ClaimsPrincipal(identity);

                await HttpContext.SignInAsync(principal);
                return RedirectToAction("home","users");
                
            }
            return View(user);
        }
    }
}
