﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Asp.net_Core.Models;
using Microsoft.AspNetCore.Hosting;

namespace Asp.net_Core.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly IHostingEnvironment _appEnvironment;

        public HomeController(ApplicationDbContext db, IHostingEnvironment appEnvironment)
        {
            _db = db;
            _appEnvironment = appEnvironment;  

        }
        public IActionResult Index()
        {
            ViewData["logo"] = "Complain Management System";
            ViewData["login"] = "Login";
            ViewData["register"] = "Register";
            ViewData["head"] = "Police station near to your finger tips!!";
            ViewData["subHead"] = "No need go to police stations,make your complain online.";
            ViewData["tipHead"] = "Give a tip for us";
            ViewData["submitTip"] = "Submit Tip";

            return View();
        }

        public IActionResult si()
        {
            ViewData["logo"] = "පැමිණිලි කළමනාකරන පද්ධතිය";
            ViewData["login"] = "ඇතුලු වන්න";
            ViewData["register"] = "ලියාපදිංචිය";
            ViewData["head"] = "පොලිස් ස්ථානයට ඇගිලි තුඩේ දුර!!";
            ViewData["subHead"] = "තවදුරටත් පොලිසියට පැමිණීමට අවශ්‍යය නෑ, ගෙදර ඉදන්ම පැමිණිල්ල ඉදිරිපත් කරන්න";
            ViewData["tipHead"] = "අපිට ඔත්තු ලබා දෙන්න";
            ViewData["submitTip"] = "ඔත්තුව යවන්න";
            return View("Index");
        }

        [AutoValidateAntiforgeryToken]
        public IActionResult tipInsert(Spy tip)
        {
            if(ModelState.IsValid)
            {
                tip.date = DateTime.Now;
                _db.Add(tip);
                _db.SaveChangesAsync();
                TempData["success"] = "Your tip sent to our officers";
                return RedirectToAction("index");
            }
            return RedirectToAction("index",tip);
        }
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
